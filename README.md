## React E-Com Example
## This is a sample E-com application built in react and redux architecture. 
## This consists of few products listed and we can add to cart and checkout.

## Steps to run the application :

### `npm install`

### `npm start`

## will be able to access the application in `http://localhost:3000/`